package com.idm.scim.ui;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import com.idm.scim.dto.Credentials;
import com.idm.scim.dto.User;
import com.idm.scim.service.IUserService;

@Named
@SessionScoped
public class LoginUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4704843110033391389L;

	final static Logger logger = Logger.getLogger(LoginUser.class);

	@Inject
	private Credentials creds;
	
	@Inject
	private User user;

	@Inject
	private IUserService userService;

	public boolean execute() {
		logger.info("Entering Validating method");
		boolean returnValue = false;
		try {
			returnValue = getUserService().validateUser(creds);
		} catch (Exception e) {
			logger.error("Invalid Credentials: " + e.getMessage());
		}
		return returnValue;
	}

	public void logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		FacesContext.getCurrentInstance().getExternalContext();
		/*
		 * try { ec.redirect(ec.getRequestContextPath() + "/login.xhtml"); } catch
		 * (IOException e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 */
		// return "login?faces-redirect=true";
	}


	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public Credentials getCreds() {
		return creds;
	}

	public void setCreds(Credentials creds) {
		this.creds = creds;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
